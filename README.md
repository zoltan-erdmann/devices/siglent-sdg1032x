# Siglent SDG1032X

Provides a few basic functions to control the SDG1032X device.

## Supported commands

### Version information

Command: `*IDN`

### Output channel state

Command: `"C<1/2>:OUTP <ON/OFF>`

### Burst wave
Command:
```
    C<Channel parameter>:BTWV 
        DLAY,1,
        STATE,ON,
        PRD,<Calculated>S,
        STPS,0,TRSR,INT TRMD,OFF,
        TIME,<Count parameter>,
        GATE_NCYC,NCYC,CARR,WVTP,
        SQUARE,
        FRQ,<Frequency parameter>HZ,
        AMP,4V,
        OFST,0V
```

## Examples

Please not that the Python example runs on version 3.8.x because pythonnet doesn't support more recent versions.

