# -*- coding: utf-8 -*-

import os
from clr import System
from System import Reflection
full_filename = os.path.abspath('../../build/bin/Debug/net472/Imperfect.Devices.Siglent.dll')
Reflection.Assembly.LoadFile(full_filename)


import time
from System.Threading import AutoResetEvent
from imperfect.devices.siglent import Sdg1032x


finished_channel_count = 0
readiness = AutoResetEvent(False)


def on_channel_ready(sender, channel):
	global finished_channel_count
	print("Channel {} ready.".format(channel))
	finished_channel_count = finished_channel_count + 1
	if(finished_channel_count >= 2):
		readiness.Set()


def main():
	hostname = "siglent"
	port = 5024
	tcp_client = System.Net.Sockets.TcpClient()

	print("Connecting to {}:{}".format(hostname, port))
	tcp_client.Connect(hostname, port)
	stream = tcp_client.GetStream()

	device = Sdg1032x(stream)
	print("ManufacturerName: {}".format(device.ManufacturerName))
	print("Name: {}".format(device.Name))
	print("SerialNumber: {}".format(device.SerialNumber))

	device.ChannelReady += on_channel_ready
	device.SetImpulse(1, 10, 0.5)
	time.sleep(1)
	device.SetImpulse(2, 10, 0.5)

	readiness.WaitOne()


main()
