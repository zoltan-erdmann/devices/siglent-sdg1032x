// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using imperfect.devices.siglent.command;

namespace imperfect.devices.siglent
{
	public delegate void ChannelReadyHandler(Sdg1032x sender, int channel);
	
    public class Sdg1032x
    {
        public event ChannelReadyHandler ChannelReady;
		
		private readonly CommandExecutor commandExecutor;
		
		public string ManufacturerName { get; }
		public string Name { get; }
		public string SerialNumber { get; }

		public Sdg1032x(Stream stream)
		{
			commandExecutor = new CommandExecutor(stream);
			commandExecutor.Call(new IdentityCommand());
			// Trimming an unknown first character.
			var response = commandExecutor.WaitAndReadResponse()?.Substring(1);
			var parts = response?.Split(',');

			ManufacturerName = parts?[0];
			Name = parts?[1];
			SerialNumber = parts?[2];
		}

		public void OpenChannel(int channel)
		{
			AssertChannel(channel);
			commandExecutor.Call(new SetChannelStateCommand
			{
				Channel = channel,
				State = State.On
			});
		}

		public void CloseChannel(int channel)
		{
			AssertChannel(channel);
			commandExecutor.Call(new SetChannelStateCommand
			{
				Channel = channel,
				State = State.Off
			});
		}

		private void AssertChannel(int channel)
		{
			if (!IsValidChannel(channel))
			{
				throw new ArgumentException(
					$"The provided channel '{channel}' is invalid. Channel should be 1 ore 2");
			}
		}

		private bool IsValidChannel(int channel)
		{
			return channel == 1 || channel == 2;
		}

		public void SetImpulse(int channel, int count, float frequency)
		{
			var estimatedTime = (int) Math.Ceiling(1.0 / frequency * count);
			
			commandExecutor.Call(new BurstWaveCommand
			{
				Channel = channel,
				Count = count,
				Frequency = frequency,
				OffsetVoltage = 2,
				VoltageAmplitude = 4,
				State = State.On
			});
			RegisterReadyEmitter(channel, estimatedTime);
		}

		private void RegisterReadyEmitter(int channel, int estimatedTimeInSecond)
		{
			Task.Run(() =>
			{
				// Adding 1 second to make sure the last impulse gets sent before closung the channel.
				Thread.Sleep((estimatedTimeInSecond + 1) * 1000);
				ChannelReady?.Invoke(this, channel);
			});
		}
    }
}
