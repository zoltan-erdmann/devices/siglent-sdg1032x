﻿// SPDX-License-Identifier: LGPL-3.0-only

using System.Text;

namespace imperfect.devices.siglent.command
{
	internal abstract class Command
    {
        public abstract string CommandString { get; }

        public override string ToString()
        {
            return $"{CommandString}\n";
        }

        public byte[] GetBytes()
        {
            return Encoding.ASCII.GetBytes(ToString());
        }
    }
}
