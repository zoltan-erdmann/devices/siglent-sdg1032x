// SPDX-License-Identifier: LGPL-3.0-only

namespace imperfect.devices.siglent.command
{
	internal class SetChannelStateCommand : Command
	{
		public override string CommandString => $"C{Channel}:OUTP {State}";
		
		public State State { get; set; }
		public int Channel { get; set; }
		
	}
}
