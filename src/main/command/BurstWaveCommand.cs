// SPDX-License-Identifier: LGPL-3.0-only

using System.Globalization;
using static System.String;

namespace imperfect.devices.siglent.command
{
	internal class BurstWaveCommand : Command
	{
		public override string CommandString => $"C{Channel}:BTWV " +
		                                        $"STATE,{State}," +
												"PLRT,NEG," +
												"TRSR,MAN," +
												"MTRIG," +
		                                        $"TIME,{Count}," +
												"GATE_NCYC,NCYC," +
		                                        "CARR,WVTP,SQUARE," +
		                                        Format(CultureInfo.InvariantCulture, "FRQ,{0:0.##}HZ,", Frequency) +
		                                        $"AMP,{VoltageAmplitude}V," +
		                                        $"OFST,{OffsetVoltage}V";
		
		public int Channel { get; set; }
		public int Count { get; set; }
		public double Frequency { get; set; }
		public double OffsetVoltage { get; set; }
		public double VoltageAmplitude { get; set; }
		public State State { get; set; }

	}
}
