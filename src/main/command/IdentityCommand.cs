﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace imperfect.devices.siglent.command
{
	internal class IdentityCommand : Command
    {
        public override string CommandString => "*IDN?";
    }
}
