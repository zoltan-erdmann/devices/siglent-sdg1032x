// SPDX-License-Identifier: LGPL-3.0-only

namespace imperfect.devices.siglent.command
{
	public enum State
	{
		On,
		Off
	}
}
