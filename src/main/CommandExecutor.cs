﻿// SPDX-License-Identifier: LGPL-3.0-only

using System.Diagnostics;
using System.IO;
using imperfect.devices.siglent.command;

namespace imperfect.devices.siglent
{
	internal class CommandExecutor
	{
		private const string LogPrefix = "SDG1032X";
		
		private readonly Stream stream;
		
		public CommandExecutor(Stream stream)
		{
			this.stream = stream;
			var welcomeLine = new StreamReader(stream).ReadLine();
			Trace.TraceInformation($"{welcomeLine}");
		}
		
		public void Call(Command command)
		{
			Trace.TraceInformation($"{LogPrefix} request: {command}");
			byte[] data = command.GetBytes();
			stream.Write(data, 0, data.Length);
		}
		
		public string WaitAndReadResponse()
		{
			var response = new StreamReader(stream).ReadLine();
			Trace.TraceInformation($"{LogPrefix} response: {response}");
			return response;
		}
	}
}
