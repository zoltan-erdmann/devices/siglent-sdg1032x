// SPDX-License-Identifier: LGPL-3.0-only

using imperfect.devices.siglent;
using NUnit.Framework;
using System;
using System.IO;
using System.Threading;

namespace Imperfect.Devices.Siglent
{
	public class Tests
	{

		[SetUp]
		public void Setup()
		{
		}

		[Test]
		public void TestEventTimeEstimation()
		{
			Stream stream = new MemoryStream();
			AutoResetEvent readyEvent = new AutoResetEvent(false);

			int count = 20;
			float frequency = 10.0f;
			int esimatedSeconds = (int)Math.Ceiling(1.0f / frequency * count);
			int channelToTest = 1;
			DateTime endTimestamp = DateTime.MinValue;

			Sdg1032x data = new Sdg1032x(stream);
			data.ChannelReady += (_, channel) =>
			{
				endTimestamp = DateTime.Now;
				readyEvent.Set();
			};
			DateTime startTimestamp = DateTime.Now;
			data.SetImpulse(channelToTest, count, frequency);
			readyEvent.WaitOne(esimatedSeconds * 2000);

			Assert.AreNotEqual(endTimestamp, DateTime.MinValue);
			Assert.Less(endTimestamp.Subtract(startTimestamp).TotalSeconds, esimatedSeconds + 2);
		}
	}
}
